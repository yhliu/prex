package com.prex.common.log.feign;

import com.prex.base.api.entity.SysLog;
import com.prex.common.core.utils.R;
import com.prex.common.log.feign.factory.RemoteLogFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Classname RemoteLogService
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-24 10:53
 * @Version 1.0
 */
@FeignClient(contextId = "remoteLogService", value = "prex-system-base-server", fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteLogService {

    @PostMapping(value = "/log")
    R saveLog(@RequestBody SysLog sysLog);
}
